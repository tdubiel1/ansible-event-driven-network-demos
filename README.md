# Ansible-Event-Driven-Network-Demos
The following demos leverage Ansible Event Driven Automation. Please note the EDA is tech preview and this repo will update as new versions of EDA are released.
* Streaming Telemetry Dashboard
* No shut ports
* Manage Config Drift 
    * This demo has dependencies on running the Cisco Compliance and Validation demo first. This demo provides config backups for comparing the running config to the intended config.
    * https://gitlab.com/redhatautomation/cisco-compliance-remediation-validation.git


## Getting started
The following links will explain how to install EDA and the other components used in the demo.

### Event-Driven Ansible install:
Please note this code is the developer tech preview.
https://github.com/ansible/event-driven-ansible

### Telegraf Collector Install
https://docs.influxdata.com/telegraf/v1.21/introduction/installation/

Please see this repo for my example telegraf.toml file.

### Kafka Install
https://kafka.apache.org/quickstart

## Controller as Code  (WIP)
Currently this repo contains only the necessary rulebooks, playbooks, templates etc for the afore mwentioned demo. Check back soon for the controller as code playbooks to build the AAP controller job templates and surveys easier in your own environement.

